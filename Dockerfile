# Pull thermoengine pkg from docker container
FROM registry.gitlab.com/enki-portal/thermoengine:master

# Copy local app to container
COPY . ${HOME}/work

# Reset permissions to allow thermoengine to run
USER root
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}

# Enable settings for cloud apps
USER root
RUN pip install --no-cache-dir appmode

# Enable nbextension for appmode
RUN jupyter nbextension enable --py --sys-prefix appmode

# Enable serverextension for appmode (using jupyter server)
RUN jupyter server extension enable --sys-prefix appmode

USER ${NB_USER}

# Install app inside container
# WORKDIR ${HOME}/app
# RUN make devinstall
# Olivine-Orthopyroxene-Spinel geothermo-oxybarometer

New Update!: Now choose between using the Fe-Mg olivine-spinel geothermometer from Jianping et al. (1995) or the Al-in-olivine geothermometer from Wan et al. (2008) and Coogan et al. (2014). Note: To use the Al-in-olivine geothermometer, an Al2O3 wt.% >0.00 is needed for all olivine and spinel data - if these cells have a value of 0.00 the code will stop running once it reaches the cell!

Welcome! This project uses either the olivine-spinel geothermometer from Jianping et al. (1995), the Al-in-olivine geothermometer from Coogan et al. (2014) and Wan et al. (2008) and the olivine-orthopyroxene-spinel geo-oxybarometer from Sack and Ghiorso, (1991a, b, 1994a, b, c). The calculators in the Jupyter notebook are built upon the ThermoEngine package from the ENKI project.

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7682607.svg)](https://doi.org/10.5281/zenodo.7682607)


Instructions for using the Jupyter notebook:
1. Download the 'oos_input.xslx' file. In this file, you will add the chemical data and calculation conditions (i.e. pressure, and temperature if you previously calculated it). DO NOT RENAME THIS FILE!
2. In 'oos_input.xslx', you can label the rows (column B) however you wish. In columns C – L, add the oxide data for spinel (in weight %). In M - U, add data for orthopyroxene, and in V - AD, add data for olivine. All compositional cells (C - AD) require a value. If you do not have a value, for example, you did not measure P2O5 wt.% in olivine, put a 0 in that cell. 
3. Add in calculation conditions. In cell AJ add the pressure (in bars) that the minerals formed at. Cell AK allows you to input a temperature (in kelvin) to use for the geobarometry calculations; if you do not have a previously determined temperature you would like to use, leave this blank, and the notebook will calculate a temperature for you. 
4. Go back to GitLab and click the 'launch binder' tab near the bottom of the page, which will take you to the repository interface. Please note that it can take a few minutes for binder to launch the first time you use it on your device, so be patient!
5. Once launched, you will be brought to the ENKI-server page. You will likely get an error saying 'Directory not found,’ which you can dismiss. Then, hit the green button at the bottom of the screen called 'Close this screen' to be taken to JupyterLab.
6. You are now in JupyterLab! The main screen in front of you is the 'Launcher'; this is where the notebook will appear. To the left of this, you will see a folder titled 'work'; open this folder. Open it. There will be five files in the folder; two of which are Jupyter notebook files ending in .ipynb. One file, titled "OOS Fe-Mg spinel olivine.ipynb" uses the olivine-spinel geothermometer from Jianping et al. (1995), while the file titled "OOS Al-in-olivine.ipynb" uses the Al-in-olivine geothermometer from Coogan et al. (2014) and Wan et al. (2008). Both notebooks use the same geobarometer, the olivine-orthopyroxene, spinel barometer from Sack and Ghiorso. Double-click on the file of interest. 
7. Before running the notebook, you must upload your version of oos_input.xslx to Jupyter Lab. To do this, go back to the left panel where you launched the notebook, and hit the third button from the left with the upwards-facing arrow symbol (if you hover over it, it will say 'Upload files'). You will get a pop-up asking if you want to overwrite the oos_input file that already exists; say yes.
8. Return to the notebook tab – it’s time to run it! Don't worry about all the lines of code you see; you will not have to interact with them. To run the code, hit the play button in the tab at the top (it looks like an arrowhead). If you hover over the symbol, it will say, 'Run the selected cells and advance.’
9. Scroll to the bottom, and hit the 'Upload' button. Navigate to your oos_input.xslx file and select it. Hit continue.
10. The calculators are now running! A file called 'oos_output.xslx' will begin to generate. At the bottom of the screen, you will see a pane with information about your session. This includes the kernel used and the amount of memory used to run the calculations. If next to the 'Python3 (ipykernel)' it says 'Busy', then the calculations are running. Wait until it says 'Idle' before downloading the output file to ensure you will have all the results.
11. The output file will contain all the initial data you used and the results for each row in columns AE - AI. In column AE you will find the temperature (in Celsius) calculated by the olivine-spinel geothermometer. If you input your own temperature, the output will simply be the temperature in Celsius you listed. In columns AF – AI, you will find the oxybarometer results. These include the absolute log fO2 (in column AF), the fO2 relative to the Quartz-Fayalite-Magnetite buffer as defined by Frost (1991), listed as 'd QFM' (in column AG), the fO2 relative to the Iron-Wustite buffer as defined by Euster and Wones (1982), listed as 'd IW' (in column AH), and the fO2 relative to the Nickle-Nickle-Oxide buffer as defined by Huebner and Sato (1970), listed as 'd NNO' (in column AI).
12. The Fe-Mg partition coefficient value of olivine and pyroxene is shown in column AL. 

If you use the results derived from this app in publications, please cite it using the DOI badge indicated.

    
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/SophieBenaroya%2Folivine-opx-spinel-geothermo-oxybarometer/HEAD)

References: 

Coogan L. A., Saunders A. D., and Wilson R. N. 2014. Aluminum-in-olivine thermometry of primitive basalts: Evidence of an anomalously hot mantle source for large igneous provinces. Chemical Geology 368:1–10.

Frost BR (1991) Introduction to oxygen fugacity and its petrologic signifi cance. Rev Mineral Geochem 25:1-9")

Herd, C.D.K., 2008. Basalts as Probes of Planetary Interior Redox State. Reviews in Mineralogy and Geochemistry 68, 527–553. https://doi.org/10.2138/rmg.2008.68.19

Huebner JS, Sato M (1970) Oxygen fugacity-temperature relationships of manganese oxide and nickel oxide buffers. Am Mineral 55:934-952

Jianping, L., Kornprobst, J., Vielzeuf, D., Fabriès, J., 1995. An improved experimental calibration of the olivine-spinel geothermometer. Chin. J. of Geochem. 14, 68–77. https://doi.org/10.1007/BF02840385

Myers, J., Eugster, H.P., 1983. The system Fe-Si-O: Oxygen buffer calibrations to 1,500K. Contr. Mineral. and Petrol. 82, 75–90. https://doi.org/10.1007/BF00371177

Sack, R.O., Ghiorso, M.S., 1994a. Thermodynamics of multicomponent pyroxenes: I. Formulation of a general model. Contr. Mineral. and Petrol. 116, 277–286. https://doi.org/10.1007/BF00306497

Sack, R.O., Ghiorso, M.S., 1994b. Thermodynamics of multicomponent pyroxenes: II. Phase relations in the quadrilateral. Contr. Mineral. and Petrol. 116, 287–300. https://doi.org/10.1007/BF00306498

Sack, R.O., Ghiorso, M.S., 1994c. Thermodynamics of multicomponent pyroxenes: III. Calibration of Fe2+(Mg)-1, TiAl2(MgSi2)-1, TiFe23+(MgSi2)-1, AlFe3+(MgSi)-1, NaAl(CaMg)-1, Al2(MgSi)-1 and Ca(Mg)-1 exchange reactions between pyroxenes and silicate melts. Contr. Mineral. and Petrol. 118, 271–296. https://doi.org/10.1007/BF00306648

Sack, R.O., Ghiorso, M.S., 1991a. Chromian spinels as petrogenetic indicators: Thermodynamics and petrological applications. American Mineralogist 76, 827–847.

Sack, R.O., Ghiorso, M.S., 1991b. An internally consistent model for the thermodynamic properties of Fe−Mg-titanomagnetite-aluminate spinels. Contr. Mineral. and Petrol. 106, 474–505. https://doi.org/10.1007/BF00321989

Wan Z., Coogan L. A., and Canil D. 2008. Experimental calibration of aluminum partitioning between olivine and spinel as a geothermometer. American Mineralogist 93:1142–1147.



